# krungsri-test

API for register, login and get information of customer (Krungsri-Test)

Security of oAuth2 template and used in with JWT

API that do not need use Authorization

1. Register customer

2. Login customer

API that do need use Authorization

1. Refresh token

2. Get information of customer

All documents are in the name is documents folder.

## Getting started

Clone this project with master branch.

## How to pack war file

1. Install all environment such JDK, Maven and set path JAVA_HOME and MAVEN_HOME

2. Goto directory level same pom.xml file

3. Run command

```
mvn clean install
```

4. If build success (war file build in target folder) name is backend-customer-rest-0.0.1.war

5. Run war file

```
java -jar backend-customer-rest-0.0.1.war
```

6. Can test API Let's started

URL LOCAL: localhost:9091/backend-customer-rest/auth/register

URL PUBLIC: http://jcloudprod.com:9091/backend-customer-rest/auth/register

## If you any problems or questions, please contact

email: piyatat.jiangsiripun@gmail.com
tel: 082-787-0911 (เต้ย)
