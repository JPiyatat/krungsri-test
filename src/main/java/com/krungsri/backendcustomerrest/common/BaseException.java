package com.krungsri.backendcustomerrest.common;

import com.krungsri.backendcustomerrest.util.enums.InternalErrorCode;

public class BaseException extends Exception {
	
	private ErrorCode errorCode;
	
	public BaseException(ErrorCode errorCode) {
		super(errorCode.getErrorMessage());
		this.errorCode = errorCode;
	}
	
	public BaseException(InternalErrorCode error) {
		super(error.getMessage());
		this.errorCode = new ErrorCode(error.getCode(), error.getMessage());
	}
	
	public ErrorCode getErrorCode() {
	    return this.errorCode;
	}

	public BaseException(InternalErrorCode error, String message) {
	    super(error.getMessage() + " " + message);
	    this.errorCode = new ErrorCode(error.getCode(), error.getMessage() + message);
	}

}
