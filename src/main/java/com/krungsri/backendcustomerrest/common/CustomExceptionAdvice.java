package com.krungsri.backendcustomerrest.common;

import com.krungsri.backendcustomerrest.util.enums.InternalErrorCode;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
@RestController
public class CustomExceptionAdvice {

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<T> handleConflict(BaseException e, HttpServletResponse response)
      throws IOException {

      return new ResponseEntity(e.getErrorCode(), HttpStatus.BAD_REQUEST);
  }
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<T> handleInvalidRequest(MethodArgumentNotValidException e, HttpServletResponse response)
      throws IOException {

	  ErrorCode errorCode = new ErrorCode(InternalErrorCode.E_9701.getCode(), InternalErrorCode.E_9701.getMessage());

      return new ResponseEntity(errorCode, HttpStatus.BAD_REQUEST);
  }

//  @Override
//  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
//                                                                HttpHeaders headers, HttpStatus status, WebRequest request) {
//
//    Map<String, String> errors = new HashMap<>();
//    ex.getBindingResult().getAllErrors().forEach((error) ->{
//
//      String fieldName = ((FieldError) error).getField();
//      String message = error.getDefaultMessage();
//      errors.put(fieldName, message);
//    });
//
//    return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
//  }
  
}