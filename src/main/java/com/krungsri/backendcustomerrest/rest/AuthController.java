package com.krungsri.backendcustomerrest.rest;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.dto.BaseResponse;
import com.krungsri.backendcustomerrest.dto.auth.request.LoginRequest;
import com.krungsri.backendcustomerrest.dto.register.request.RegisterRequest;
import com.krungsri.backendcustomerrest.service.AuthenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import com.krungsri.backendcustomerrest.util.Mapping;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(Mapping.AUTH)
public class AuthController {

    private final static Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private AuthenService authenService;

    @PostMapping(Mapping.LOGIN)
    public BaseResponse authenticateUser(@RequestBody @Valid LoginRequest request) throws BaseException {

        logger.info("============== Start Api Auth authenticateUser ==============");
        logger.info("input: request={} ", request);

        final BaseResponse response = this.authenService.authen(request).throwBaseExceptionIfError();

        logger.info("output: response={}", response);
        logger.info("============== End Api Auth authenticateUser ==============");

        return response;

    }

    @GetMapping(Mapping.REFRESH_TOKEN)
    public BaseResponse refreshToken(HttpServletRequest request) throws Exception {

        logger.info("============== Start Api Auth refreshToken ==============");
        logger.info("input: request={} ", request);

        final BaseResponse response = this.authenService.refreshToken(request).throwBaseExceptionIfError();

        logger.info("output: response={}", response);
        logger.info("============== End Api Auth refreshToken ==============");

        return response;
    }

    @PostMapping(Mapping.REGISTER)
    public BaseResponse registerUser(@Valid @RequestBody RegisterRequest request) throws BaseException {

        logger.info("============== Start Api Auth registerUser ==============");
        logger.info("input: request={} ", request);

        final BaseResponse response = this.authenService.register(request).throwBaseExceptionIfError();

        logger.info("output: response={}", response);
        logger.info("============== End Api Auth registerUser ==============");

        return response;

    }

}
