package com.krungsri.backendcustomerrest.rest;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.dto.BaseResponse;
import com.krungsri.backendcustomerrest.dto.customer.request.CustomerRequest;
import com.krungsri.backendcustomerrest.service.CustomerService;
import com.krungsri.backendcustomerrest.util.Mapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(Mapping.API + Mapping.CUSTOMER)
public class CustomerController {

    private final static Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;

    @GetMapping(Mapping.INFORMATION)
    public BaseResponse getInformationCustomer(@RequestBody @Valid CustomerRequest request) throws BaseException {

        logger.info("============== Start Api Customer getInformationCustomer ==============");
        logger.info("input: request={} ", request);

        final BaseResponse response = this.customerService.getInformation(request).throwBaseExceptionIfError();

        logger.info("output: response={}", response);
        logger.info("============== End Api Customer getInformationCustomer ==============");

        return response;
    }

}
