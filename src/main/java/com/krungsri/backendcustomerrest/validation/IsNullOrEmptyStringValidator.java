package com.krungsri.backendcustomerrest.validation;

import com.krungsri.backendcustomerrest.validation.constraints.IsNullOrEmpty;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsNullOrEmptyStringValidator implements ConstraintValidator<IsNullOrEmpty, String> {

    @Override
    public void initialize(IsNullOrEmpty constraintAnnotation) {
        
    }
    
    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return validate(value);
    }
    
    private boolean validate(String value) {
        if(null == value || "".equals(value.trim()))
            return false;
        
        return true;
    }
    
}
