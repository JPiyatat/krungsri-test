package com.krungsri.backendcustomerrest.service.impl;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.common.ErrorCode;
import com.krungsri.backendcustomerrest.dto.customer.request.CustomerRequest;
import com.krungsri.backendcustomerrest.dto.customer.response.CustomerData;
import com.krungsri.backendcustomerrest.dto.customer.response.CustomerResponse;
import com.krungsri.backendcustomerrest.dto.register.response.RegisterData;
import com.krungsri.backendcustomerrest.dto.register.response.RegisterResponse;
import com.krungsri.backendcustomerrest.model.Customer;
import com.krungsri.backendcustomerrest.repository.CustomerRepository;
import com.krungsri.backendcustomerrest.service.CustomerService;
import com.krungsri.backendcustomerrest.util.CommonUtils;
import com.krungsri.backendcustomerrest.util.enums.InternalErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final static Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public CustomerResponse getInformation(CustomerRequest request) throws BaseException {
        CustomerResponse response = null;

        try {
            // Check username
            logger.info("input for customerRepository.findByUsername request.getUsername = {}", request.getUsername());
            Customer customer = customerRepository.findByUsername(request.getUsername()).orElse(null);

            if(CommonUtils.chkIsBlankNullObject(customer)) {
                throw new BaseException(InternalErrorCode.E_1104);
            }

            // map data to response
            response = mappedCustomerToResponse(customer);

        } catch (BaseException ex) {

            logger.error(ex.getMessage(), ex);
            ErrorCode errorCode = ex.getErrorCode();

            if(null == errorCode || null == errorCode.getErrorCode()) {
                throw new BaseException(InternalErrorCode.E_9901);
            }

            throw ex;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new BaseException(InternalErrorCode.E_9999);
        }

        return response;
    }

    private CustomerResponse mappedCustomerToResponse(Customer customer) throws BaseException {

        CustomerResponse response = new CustomerResponse();
        CustomerData data = new CustomerData();

        data.setRef_code(customer.getRefCode());
        data.setUsername(customer.getUsername());
        data.setFirstname(customer.getFirstname());
        data.setLastname(customer.getLastname());
        data.setAddress(customer.getAddress());
        data.setPhone(customer.getPhone());
        data.setSalary(customer.getSalary());
        data.setMember_type(customer.getMemberType().toString());

        response.setData(data);

        return response;
    }

}
