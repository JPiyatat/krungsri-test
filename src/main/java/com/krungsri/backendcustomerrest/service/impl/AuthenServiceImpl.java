package com.krungsri.backendcustomerrest.service.impl;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.common.ErrorCode;
import com.krungsri.backendcustomerrest.dto.auth.request.LoginRequest;
import com.krungsri.backendcustomerrest.dto.auth.response.LoginData;
import com.krungsri.backendcustomerrest.dto.auth.response.LoginResponse;
import com.krungsri.backendcustomerrest.dto.register.request.RegisterRequest;
import com.krungsri.backendcustomerrest.dto.register.response.RegisterData;
import com.krungsri.backendcustomerrest.dto.register.response.RegisterResponse;
import com.krungsri.backendcustomerrest.model.Customer;
import com.krungsri.backendcustomerrest.repository.CustomerRepository;
import com.krungsri.backendcustomerrest.security.TokenProvider;
import com.krungsri.backendcustomerrest.service.AuthenService;
import com.krungsri.backendcustomerrest.util.CommonUtils;
import com.krungsri.backendcustomerrest.util.DateUtils;
import com.krungsri.backendcustomerrest.util.enums.InternalErrorCode;
import com.krungsri.backendcustomerrest.util.enums.MemberType;
import io.jsonwebtoken.impl.DefaultClaims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthenServiceImpl implements AuthenService {

    private final static Logger logger = LoggerFactory.getLogger(AuthenServiceImpl.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public LoginResponse authen(LoginRequest request) throws BaseException {

        LoginResponse response = new LoginResponse();
        LoginData data = new LoginData();

        try {
            // Check username
            Customer customer = customerRepository.findByUsername(request.getUsername()).orElse(null);

            if(!CommonUtils.chkIsBlankNullObject(customer)) {

                logger.info("input for authenticationManager.authenticate request.getUsername = {}",
                        null == request.getUsername() ? null : request.getUsername());

                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                request.getUsername(),
                                request.getPassword()
                        )
                );

                logger.info("output for authenticationManager.authenticate authentication.getName = {}",
                        null == authentication.getName() ? null : authentication.getName());

                SecurityContextHolder.getContext().setAuthentication(authentication);
                String token = tokenProvider.createToken(authentication);

                logger.info("output for tokenProvider.createToken token = {}",
                        null == token ? null : token);

                data.setAccess_token(token);
                data.setData_encrypt(tokenProvider.getDataEncryptFromToken(token));
                data.setUsername(customer.getUsername());
                data.setId(customer.getId().toString());
                response.setData(data);
            }else {
                throw new BaseException(InternalErrorCode.E_1104);
            }
        } catch (BaseException ex) {

            logger.error(ex.getMessage(), ex);
            ErrorCode errorCode = ex.getErrorCode();

            if(null == errorCode || null == errorCode.getErrorCode()) {
                throw new BaseException(InternalErrorCode.E_9901);
            }

            throw ex;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new BaseException(InternalErrorCode.E_9999);
        }

        return response;
    }

    @Override
    public LoginResponse refreshToken(HttpServletRequest request) throws BaseException {
        LoginResponse response = new LoginResponse();
        LoginData data = new LoginData();

        try {
            String jwt = CommonUtils.getJwtFromRequest(request);
            // From the HttpRequest get the claims
            DefaultClaims claims = (DefaultClaims) request.getAttribute("claims");

            Map<String, Object> expectedMap = getMapFromIoJsonwebtokenClaims(claims);

            String aud = expectedMap.get("aud").toString();
            String subject = expectedMap.get("sub").toString();

            String refreshToken = tokenProvider.doGenerateRefreshToken(expectedMap, aud, subject);

            Customer customer = customerRepository.findById(Long.parseLong(subject)).orElse(null);
            if(CommonUtils.chkIsBlankNullObject(customer))
                throw new BaseException(InternalErrorCode.E_1104);

            data.setAccess_token(refreshToken);
            data.setData_encrypt(tokenProvider.getDataEncryptFromToken(refreshToken));
            data.setUsername(customer.getUsername());
            data.setId(customer.getId().toString());
            response.setData(data);

        } catch (BaseException ex) {

            logger.error(ex.getMessage(), ex);
            ErrorCode errorCode = ex.getErrorCode();

            if(null == errorCode || null == errorCode.getErrorCode()) {
                throw new BaseException(InternalErrorCode.E_9901);
            }

            throw ex;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new BaseException(InternalErrorCode.E_9999);
        }

        return response;
    }

    private Map<String, Object> getMapFromIoJsonwebtokenClaims(DefaultClaims claims) {
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        for(Map.Entry<String, Object> entry : claims.entrySet()) {
            expectedMap.put(entry.getKey(), entry.getValue());
        }
        return expectedMap;
    }

    @Override
    public RegisterResponse register(RegisterRequest request) throws BaseException {

        RegisterResponse response;

        try {
            // Check duplicate username
            logger.info("input for customerRepository.findByUsername request.getUsername = {}", request.getUsername());
            Customer chkCustomer = customerRepository.findByUsername(request.getUsername()).orElse(null);

            if(!CommonUtils.chkIsBlankNullObject(chkCustomer)) {
                throw new BaseException(InternalErrorCode.E_1105);
            }

            // validate optional field
            validateOptional(request);

            // set data to customer model
            Customer customer = mappedRegisterRequestToCustomer(request);

            customerRepository.save(customer);

            // map data to response
            response = mappedCustomerToResponse(customer);

        } catch (BaseException ex) {

            logger.error(ex.getMessage(), ex);
            ErrorCode errorCode = ex.getErrorCode();

            if(null == errorCode || null == errorCode.getErrorCode()) {
                throw new BaseException(InternalErrorCode.E_9901);
            }

            throw ex;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new BaseException(InternalErrorCode.E_9999);
        }

        return response;
    }

    private Customer mappedRegisterRequestToCustomer(RegisterRequest request) throws BaseException {

        Customer customer = new Customer();
        String genCode;
        String last4Digits = null;

        try{
            genCode = DateUtils.convertDateToString(new Date(), DateUtils.FORMAT_YYYYMMDD_1);

            if(request.getPhone().length() > 4) {
                last4Digits = request.getPhone().substring(request.getPhone().length() - 4);
            }

            if(!CommonUtils.chkIsBlankNullObject(last4Digits)) {
                genCode += last4Digits;
            }

        }catch(Exception e) {
            genCode = null;
        }

        customer.setRefCode(genCode);
        customer.setUsername(request.getUsername());
        customer.setPassword(CommonUtils.chkIsBlankNullObject(request.getPassword()) ? null : passwordEncoder.encode(request.getPassword()));
        customer.setFirstname(request.getFirstname());
        customer.setLastname(request.getLastname());
        customer.setAddress(request.getAddress());
        customer.setPhone(request.getPhone());
        customer.setSalary(request.getSalary());

        // Check salary set member type
        if(request.getSalary().compareTo(new BigDecimal(50000)) > 0) {
            customer.setMemberType(MemberType.Platinum);
        }else if(request.getSalary().compareTo(new BigDecimal(30000)) >= 0 && request.getSalary().compareTo(new BigDecimal(50000)) <= 0) {
            customer.setMemberType(MemberType.Gold);
        }else if(request.getSalary().compareTo(new BigDecimal(15000)) >= 0 && request.getSalary().compareTo(new BigDecimal(30000)) < 0) {
            customer.setMemberType(MemberType.Silver);
        }else {
            // Reject if salary < 15,000
            throw new BaseException(InternalErrorCode.E_1106);
        }

        return customer;
    }

    private RegisterResponse mappedCustomerToResponse(Customer customer) throws BaseException {

        RegisterResponse response = new RegisterResponse();
        RegisterData data = new RegisterData();

        data.setUsername(customer.getUsername());
        data.setRef_code(customer.getRefCode());
        data.setMember_type(customer.getMemberType().toString());

        response.setData(data);

        return response;
    }

    private void validateOptional(RegisterRequest request) throws BaseException {
        if(CommonUtils.chkIsBlankNullObject(request.getSalary()) || request.getSalary().compareTo(new BigDecimal(0)) <= 0) {
            throw new BaseException(InternalErrorCode.E_9701);
        }
    }

}
