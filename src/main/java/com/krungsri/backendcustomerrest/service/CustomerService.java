package com.krungsri.backendcustomerrest.service;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.dto.customer.request.CustomerRequest;
import com.krungsri.backendcustomerrest.dto.customer.response.CustomerResponse;

public interface CustomerService {

    CustomerResponse getInformation(CustomerRequest request) throws BaseException;

}
