package com.krungsri.backendcustomerrest.service;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.dto.auth.request.LoginRequest;
import com.krungsri.backendcustomerrest.dto.auth.response.LoginResponse;
import com.krungsri.backendcustomerrest.dto.register.request.RegisterRequest;
import com.krungsri.backendcustomerrest.dto.register.response.RegisterResponse;

import javax.servlet.http.HttpServletRequest;

public interface AuthenService {

    LoginResponse authen(LoginRequest request) throws BaseException;
    LoginResponse refreshToken(HttpServletRequest request) throws BaseException;
    RegisterResponse register(RegisterRequest request) throws BaseException;

}
