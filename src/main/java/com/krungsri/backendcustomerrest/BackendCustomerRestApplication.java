package com.krungsri.backendcustomerrest;

import com.krungsri.backendcustomerrest.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class BackendCustomerRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendCustomerRestApplication.class, args);
	}

	@PostConstruct
	void started() {
		// set JVM timezone as UTC
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Bangkok"));
	}

}
