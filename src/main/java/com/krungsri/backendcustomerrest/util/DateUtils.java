package com.krungsri.backendcustomerrest.util;

import com.krungsri.backendcustomerrest.common.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private final static Logger logger = LoggerFactory.getLogger(DateUtils.class);

    public static String FORMAT_YYYYMMDD_1 = "yyyyMMdd";

    public static Timestamp getCurrentTimestamp() throws BaseException {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        return timestamp;
    }

    public static String convertDateToString(Date date, String format) throws BaseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String strDate = null;

        if(CommonUtils.chkIsBlankNullObject(date)) {
            return null;
        }

        try {
            strDate = sdf.format(date);
        }catch (Exception e) {
            sdf = new SimpleDateFormat(FORMAT_YYYYMMDD_1);
            strDate = sdf.format(date);
            logger.error("convertDateToString: " + e.getMessage(), e);
        }

        return strDate;

    }

    public static Date convertStringToDate(String strDate, String format) throws BaseException {

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = null;

        try {
            date = (Date) sdf.parse(strDate);
            return date;
        }catch (ParseException e) {
            logger.error("convertStringToDate: " + e.getMessage(), e);
        }

        return date;
    }

}
