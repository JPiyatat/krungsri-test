package com.krungsri.backendcustomerrest.util.enums;

public enum MemberType {

    Platinum,
    Gold,
    Silver

}
