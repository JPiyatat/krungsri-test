package com.krungsri.backendcustomerrest.util.enums;

public enum InternalErrorCode {
    
    // Technical errors
    E_0000("0000", "Success"),
    E_9999("9999", "Unknown error"),
    E_9901("9901", "Internal exception failure"),
    E_9801("9801", "Database connection timeout"),
    E_9701("9701", "Invalid request parameter"),
    E_7000("7000", "Internal connection timeout."),

    // Warning code
    //W_2101("2101", "Scan completed but no start time was found. Please check your timesheet on the website."),
    //W_2102("2102", "Please check Going Out time. Because no data was found"),
    //W_2103("2103", "Please check Direct Visit time. Because no data was found"),
    
    // Business errors
    E_1101("1101", "Name is duplicated."),
    E_1102("1102", "Data not found."),
	E_1103("1103", "Data encrypt is invalid."),
	E_1104("1104", "Username not exists"),
    E_1105("1105", "Username already exists"),
    E_1106("1106", "Salary must more than or equal 15,000 baht");
    
    private final String code;
    private final String message;
    
    private InternalErrorCode(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
    
    public static InternalErrorCode getEnumInternalErrorCode(final String code) {
        for(InternalErrorCode enumInternalErrorCode : InternalErrorCode.values()) {
            if(enumInternalErrorCode.getCode().equals(code)) {
                return enumInternalErrorCode;
            }
        }
        
        return InternalErrorCode.E_9999;
    }

}
