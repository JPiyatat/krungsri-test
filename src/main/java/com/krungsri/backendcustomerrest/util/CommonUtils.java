package com.krungsri.backendcustomerrest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class CommonUtils {

    private final static Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    public static boolean chkIsBlankNullObject(Object obj) {

        if(null == obj)
            return true;

        if(obj instanceof String) {
            String data = (String) obj;
            if("null".equalsIgnoreCase(data) || (data).trim().isEmpty()){
                return true;
            }
        }

        return false;
    }

    public static String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }

}
