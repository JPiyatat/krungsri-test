package com.krungsri.backendcustomerrest.util;

public class Mapping {

    public static final String API = "/api";
    public static final String AUTH = "/auth";
    public static final String CUSTOMER = "/customer";
    public static final String LOGIN = "/login";
    public static final String REFRESH_TOKEN = "/refreshtoken";
    public static final String REGISTER = "/register";
    public static final String INFORMATION = "/information";

}
