package com.krungsri.backendcustomerrest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.krungsri.backendcustomerrest.util.enums.MemberType;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "tbl_customer")
@Data
@SuperBuilder
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    public Customer() {};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name="ref_code")
    @Size(max = 12)
    private String refCode;

    @Column(name="username")
    @Size(max = 100)
    private String username;

    @Column(name="password")
    @Size(max = 255)
    @JsonIgnore
    private String password;

    @Column(name="firstname")
    @Size(max = 100)
    private String firstname;

    @Column(name="lastname")
    @Size(max = 100)
    private String lastname;

    @Column(name="address")
    private String address;

    @Column(name="phone")
    @Size(max = 50)
    private String phone;

    @Column(name="salary")
    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer=10, fraction=2)
    private BigDecimal salary;

    @Column(name="member_type")
    @Enumerated(EnumType.STRING)
    private MemberType memberType;

}
