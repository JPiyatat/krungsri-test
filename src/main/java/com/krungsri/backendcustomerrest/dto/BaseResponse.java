package com.krungsri.backendcustomerrest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.common.ErrorCode;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

@ToString
@SuperBuilder
@EqualsAndHashCode
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class BaseResponse {

    @NotNull
    private String code = "0000";

    @NotNull
    private String description = "Success";

	public <T extends BaseResponse> T throwBaseExceptionIfError() throws BaseException {
	    if(!"0000".equals(this.getCode())) {
	        throw new BaseException(new ErrorCode(this.getCode(), this.getDescription()));
	    }

	    return (T)this;
	}

}
