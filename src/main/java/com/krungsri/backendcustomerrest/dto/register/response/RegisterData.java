package com.krungsri.backendcustomerrest.dto.register.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterData {

    private String username;
    private String ref_code;
    private String member_type;

}
