package com.krungsri.backendcustomerrest.dto.register.response;

import com.krungsri.backendcustomerrest.dto.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RegisterResponse extends BaseResponse {

    private RegisterData data;

}
