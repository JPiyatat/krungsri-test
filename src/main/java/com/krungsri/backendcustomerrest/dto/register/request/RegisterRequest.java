package com.krungsri.backendcustomerrest.dto.register.request;

import com.krungsri.backendcustomerrest.dto.BaseRequest;
import com.krungsri.backendcustomerrest.validation.constraints.IsNullOrEmpty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RegisterRequest extends BaseRequest {

    @IsNullOrEmpty
    @Size(max = 255)
    private String password;

    @IsNullOrEmpty
    @Size(max = 100)
    private String firstname;

    @IsNullOrEmpty
    @Size(max = 100)
    private String lastname;

    private String address;

    @IsNullOrEmpty
    @Size(max = 50)
    private String phone;

    private BigDecimal salary;

}
