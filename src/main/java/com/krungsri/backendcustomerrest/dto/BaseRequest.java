package com.krungsri.backendcustomerrest.dto;

import com.krungsri.backendcustomerrest.validation.constraints.IsNullOrEmpty;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@SuperBuilder
@EqualsAndHashCode
@ToString
public class BaseRequest implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@IsNullOrEmpty
	@Size(max = 100)
	private String username;
	
	public BaseRequest() {
		super();
	}
	
	public BaseRequest(String username) {
		super();
		this.username = username;
	}

}
