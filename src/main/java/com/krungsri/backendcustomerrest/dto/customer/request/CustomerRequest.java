package com.krungsri.backendcustomerrest.dto.customer.request;

import com.krungsri.backendcustomerrest.dto.BaseRequest;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CustomerRequest extends BaseRequest {

}
