package com.krungsri.backendcustomerrest.dto.customer.response;

import com.krungsri.backendcustomerrest.dto.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CustomerResponse extends BaseResponse {

    private CustomerData data;

}
