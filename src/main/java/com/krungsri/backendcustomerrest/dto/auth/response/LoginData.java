package com.krungsri.backendcustomerrest.dto.auth.response;

import lombok.Data;

@Data
public class LoginData {

    private String access_token;
    private String token_type = "Bearer";
    private String data_encrypt;
    private String username;
    private String id;

}
