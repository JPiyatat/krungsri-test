package com.krungsri.backendcustomerrest.dto.auth.request;

import com.krungsri.backendcustomerrest.dto.BaseRequest;
import com.krungsri.backendcustomerrest.validation.constraints.IsNullOrEmpty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Size;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class LoginRequest extends BaseRequest {

    @IsNullOrEmpty
    @Size(max = 255)
    private String password;

}
