package com.krungsri.backendcustomerrest.dto.auth.response;

import com.krungsri.backendcustomerrest.dto.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class LoginResponse extends BaseResponse {

    private LoginData data;

}
