package com.krungsri.backendcustomerrest.security;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.model.Customer;
import com.krungsri.backendcustomerrest.repository.CustomerRepository;

import com.krungsri.backendcustomerrest.util.enums.InternalErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final static Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    CustomerRepository customerRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        Customer customer = null;
        try{
            customer = customerRepository.findByUsername(username)
                    .orElseThrow(() ->
                            new UsernameNotFoundException("User not found with username : " + username)
                    );
        }catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return UserPrincipal.create(customer);
    }

    @Transactional
    public UserDetails loadUserById(Long id) throws BaseException {
        Customer customer = customerRepository.findById(id).orElseThrow(
                () -> new BaseException(InternalErrorCode.E_1102)
        );

        return UserPrincipal.create(customer);
    }
}
