package com.krungsri.backendcustomerrest.security;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.util.CommonUtils;
import com.krungsri.backendcustomerrest.util.enums.InternalErrorCode;

import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwt = CommonUtils.getJwtFromRequest(request);

            if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {

                // Check exists dataEncrypt
                if(CommonUtils.chkIsBlankNullObject(request.getHeader("dataEncrypt"))) {
                    throw new BaseException(InternalErrorCode.E_9701);
                }

                Long userId = tokenProvider.getUserIdFromToken(jwt);
                String dataEncrypt = tokenProvider.getDataEncryptFromToken(jwt);

                // Check dataEncrypt from request and token
                if(!request.getHeader("dataEncrypt").equals(dataEncrypt)) {
                    throw new BaseException(InternalErrorCode.E_1103);
                }

                UserDetails userDetails = customUserDetailsService.loadUserById(userId);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch(ExpiredJwtException ex) {
            // Case expired JWT Token
            String isRefreshToken = request.getHeader("isRefreshToken");
            String requstURL = request.getRequestURL().toString();
            if(null != isRefreshToken && isRefreshToken.equals("true") && requstURL.contains("refreshtoken")) {
                allowForRefreshToken(ex, request);
            }else {
                logger.error("Could not set user authentication in security context", ex);
                response.setHeader("JWT", "TOKEN_EXPIRED");
            }
        }catch (Exception ex) {
            logger.error("Could not set user authentication in security context", ex);
        }

        filterChain.doFilter(request, response);
    }

    private void allowForRefreshToken(ExpiredJwtException ex, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                null, null, null
        );

        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        request.setAttribute("claims", ex.getClaims());
    }
}
