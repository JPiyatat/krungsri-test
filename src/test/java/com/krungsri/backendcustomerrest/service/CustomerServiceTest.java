package com.krungsri.backendcustomerrest.service;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.dto.customer.request.CustomerRequest;
import com.krungsri.backendcustomerrest.dto.customer.response.CustomerData;
import com.krungsri.backendcustomerrest.dto.customer.response.CustomerResponse;
import com.krungsri.backendcustomerrest.model.Customer;
import com.krungsri.backendcustomerrest.repository.CustomerRepository;
import com.krungsri.backendcustomerrest.service.impl.CustomerServiceImpl;
import com.krungsri.backendcustomerrest.util.DateUtils;
import com.krungsri.backendcustomerrest.util.enums.MemberType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @InjectMocks
    CustomerServiceImpl customerServiceMock;

    @Mock
    CustomerRepository customerRepository;

    CustomerRequest customerRequest;
    CustomerResponse customerResponse;
    Customer customer;

    @Before
    public void setUp() throws BaseException {

        String genDate = DateUtils.convertDateToString(new Date(), DateUtils.FORMAT_YYYYMMDD_1);

        customerRequest = CustomerRequest.builder()
                .username("test3")
                .build();

        customerResponse = CustomerResponse.builder()
                .code("0000")
                .description("Success")
                .data(CustomerData.builder()
                        .ref_code(genDate+"5678")
                        .username("test3")
                        .firstname("test3")
                        .lastname("last3")
                        .address("124")
                        .phone("0812345678")
                        .salary(new BigDecimal(50000))
                        .member_type(MemberType.Gold.toString())
                        .build())
                .build();

        customer = Customer.builder()
                .id(1L)
                .refCode(genDate+"5678")
                .username("test3")
                .password("xxxxxxxxx")
                .firstname("test3")
                .lastname("last3")
                .address("124")
                .phone("0812345678")
                .salary(new BigDecimal(50000))
                .memberType(MemberType.Gold)
                .build();
    }

    @Test
    public void testGetInformationCustomerSuccess() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(customer));

        CustomerResponse response = customerServiceMock.getInformation(customerRequest);

        Assert.assertEquals(customerResponse, response);
    }

    @Test(expected = BaseException.class)
    public void testGetInformationCustomerNotExists() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(null));

        CustomerResponse response = customerServiceMock.getInformation(customerRequest);
    }

}
