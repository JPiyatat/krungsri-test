package com.krungsri.backendcustomerrest.service;

import com.krungsri.backendcustomerrest.common.BaseException;
import com.krungsri.backendcustomerrest.dto.register.request.RegisterRequest;
import com.krungsri.backendcustomerrest.dto.register.response.RegisterData;
import com.krungsri.backendcustomerrest.dto.register.response.RegisterResponse;
import com.krungsri.backendcustomerrest.model.Customer;
import com.krungsri.backendcustomerrest.repository.CustomerRepository;
import com.krungsri.backendcustomerrest.service.impl.AuthenServiceImpl;
import com.krungsri.backendcustomerrest.util.DateUtils;
import com.krungsri.backendcustomerrest.util.enums.MemberType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenServiceTest {

    @InjectMocks
    AuthenServiceImpl authenServiceMock;

    @Mock
    CustomerRepository customerRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    RegisterRequest registerRequest;
    RegisterResponse registerResponse;
    Customer customer;
    String genDate;

    @Before
    public void setUp() throws BaseException {

        genDate = DateUtils.convertDateToString(new Date(), DateUtils.FORMAT_YYYYMMDD_1);

        registerRequest = RegisterRequest.builder()
                .username("test3")
                .password("1234")
                .firstname("test3")
                .lastname("last3")
                .address("214")
                .phone("0812345678")
                .salary(new BigDecimal(15000))
                .build();

        registerResponse = RegisterResponse.builder()
                .code("0000")
                .description("Success")
                .data(RegisterData.builder().ref_code(genDate+"5678").username("test3").member_type(MemberType.Silver.toString()).build())
                .build();

        customer = Customer.builder()
                .id(1L)
                .refCode(genDate+"5678")
                .username("test1")
                .password("xxxxxxxxx")
                .firstname("test1")
                .lastname("last1")
                .address("214")
                .phone("0812345678")
                .salary(new BigDecimal(20000))
                .memberType(MemberType.Silver)
                .build();
    }

    @Test
    public void testRegisterSuccess() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(null));
        when(passwordEncoder.encode(anyString())).thenReturn("123456789");

        RegisterResponse response = authenServiceMock.register(registerRequest);

        Assert.assertEquals(registerResponse, response);
    }

    @Test
    public void testMapCustomerSilver() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(null));
        when(passwordEncoder.encode(anyString())).thenReturn("123456789");

        registerResponse = RegisterResponse.builder()
                .code("0000")
                .description("Success")
                .data(RegisterData.builder().ref_code(genDate+"5678").username("test3").member_type(MemberType.Silver.toString()).build())
                .build();

        registerRequest = RegisterRequest.builder()
                .username("test3")
                .password("1234")
                .firstname("test3")
                .lastname("last3")
                .address("214")
                .phone("0812345678")
                .salary(new BigDecimal(15000))
                .build();

        RegisterResponse response = authenServiceMock.register(registerRequest);

        Assert.assertEquals(registerResponse, response);
        Assert.assertEquals(MemberType.Silver.toString(), response.getData().getMember_type());
    }

    @Test
    public void testMapCustomerSilver2() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(null));
        when(passwordEncoder.encode(anyString())).thenReturn("123456789");

        registerResponse = RegisterResponse.builder()
                .code("0000")
                .description("Success")
                .data(RegisterData.builder().ref_code(genDate+"5678").username("test3").member_type(MemberType.Silver.toString()).build())
                .build();

        registerRequest = RegisterRequest.builder()
                .username("test3")
                .password("1234")
                .firstname("test3")
                .lastname("last3")
                .address("214")
                .phone("0812345678")
                .salary(new BigDecimal(29999))
                .build();

        RegisterResponse response = authenServiceMock.register(registerRequest);

        Assert.assertEquals(registerResponse, response);
        Assert.assertEquals(MemberType.Silver.toString(), response.getData().getMember_type());
    }

    @Test
    public void testMapCustomerGold() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(null));
        when(passwordEncoder.encode(anyString())).thenReturn("123456789");

        registerResponse = RegisterResponse.builder()
                .code("0000")
                .description("Success")
                .data(RegisterData.builder().ref_code(genDate+"5678").username("test3").member_type(MemberType.Gold.toString()).build())
                .build();

        registerRequest = RegisterRequest.builder()
                .username("test3")
                .password("1234")
                .firstname("test3")
                .lastname("last3")
                .address("214")
                .phone("0812345678")
                .salary(new BigDecimal(30000))
                .build();

        RegisterResponse response = authenServiceMock.register(registerRequest);

        Assert.assertEquals(registerResponse, response);
        Assert.assertEquals(MemberType.Gold.toString(), response.getData().getMember_type());
    }

    @Test
    public void testMapCustomerGold2() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(null));
        when(passwordEncoder.encode(anyString())).thenReturn("123456789");

        registerResponse = RegisterResponse.builder()
                .code("0000")
                .description("Success")
                .data(RegisterData.builder().ref_code(genDate+"5678").username("test3").member_type(MemberType.Gold.toString()).build())
                .build();

        registerRequest = RegisterRequest.builder()
                .username("test3")
                .password("1234")
                .firstname("test3")
                .lastname("last3")
                .address("214")
                .phone("0812345678")
                .salary(new BigDecimal(50000))
                .build();

        RegisterResponse response = authenServiceMock.register(registerRequest);

        Assert.assertEquals(registerResponse, response);
        Assert.assertEquals(MemberType.Gold.toString(), response.getData().getMember_type());
    }

    @Test
    public void testMapCustomerPlatinum() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(null));
        when(passwordEncoder.encode(anyString())).thenReturn("123456789");

        registerResponse = RegisterResponse.builder()
                .code("0000")
                .description("Success")
                .data(RegisterData.builder().ref_code(genDate+"5678").username("test3").member_type(MemberType.Platinum.toString()).build())
                .build();

        registerRequest = RegisterRequest.builder()
                .username("test3")
                .password("1234")
                .firstname("test3")
                .lastname("last3")
                .address("214")
                .phone("0812345678")
                .salary(new BigDecimal(50001))
                .build();

        RegisterResponse response = authenServiceMock.register(registerRequest);

        Assert.assertEquals(registerResponse, response);
        Assert.assertEquals(MemberType.Platinum.toString(), response.getData().getMember_type());
    }

    @Test(expected = BaseException.class)
    public void testRegisterUsernameExists() throws BaseException {
        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(customer));

        RegisterResponse response = authenServiceMock.register(registerRequest);
    }

    @Test(expected = BaseException.class)
    public void testRegisterSalaryReject() throws BaseException {

        registerRequest = RegisterRequest.builder()
                .username("test3")
                .password("1234")
                .firstname("test3")
                .lastname("last3")
                .address("214")
                .phone("0812345678")
                .salary(new BigDecimal(14999))
                .build();

        when(customerRepository.findByUsername(anyString())).thenReturn(java.util.Optional.ofNullable(null));

        RegisterResponse response = authenServiceMock.register(registerRequest);
    }

}
